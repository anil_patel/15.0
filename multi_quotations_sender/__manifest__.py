{
    'name': 'Multi Quotations Sender',
    'author': 'Valueble IT Solution',
    'website': 'valuebleitsolution.odoo.com',
    'sequence': 1,
    'version': '1.0',
    'category': 'Apps',
    'summary': 'This module allows to send multiple sales quotation and purchase order rfq',
    'images': ['static/description/main_screenshot.png'],
    'description': """
        Send Multiple Quotation,
        Send Multiple RFQ,
        Send multiple quotation in one email,
        Mass quotation sender,
        Mass Rfq sender,
        Quotation,
        multi quote,
        multi rfq
        rfq
    """,
    'price': "20",
    'currency': 'USD',
    'depends': ["sale", "purchase"],
    'data': [
        'data/mail_template_data.xml',
        'views/sale_order_view.xml',
        'views/purchase.xml'
    ],
}
