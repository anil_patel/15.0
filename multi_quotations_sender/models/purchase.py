from odoo import models, api
from odoo.exceptions import UserError


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def send_multiple_quotations(self):

        # Check if all selected quotations are for the same partner
        if len(set(self.mapped("partner_id"))) > 1:
            raise UserError("You must select rfq for the same partner")

        # ir_model_data = self.env['ir.model.data']
        # try:
        #     template_id = ir_model_data.get_object_reference('multi_quotations_sender',
        #                                                      'email_template_multi_quotations')[1]
        # except ValueError:
        #     template_id = False
        template_id = self.env.ref('multi_quotations_sender.multi_quotations_sender_purchase_template').id

        ctx = {
            'purchase_quotations_ids': self.ids,
            'default_model': 'purchase.order',
            'default_res_id': self[0].id,
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'force_email': True
        }
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'target': 'new',
            'context': ctx,
        }
