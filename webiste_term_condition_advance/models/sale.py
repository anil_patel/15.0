# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import datetime

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    pos_term_accept = fields.Boolean(string="Pos Term Accepted?", copy=False)
    pos_term_accept_date = fields.Date(string="Pos Term Accepted Date", copy=False)
    term_templates = fields.Char(string="Term Templates")


    def check_term_condition_required(self):
        template_lst = []
        if self.order_line:
            for each in self.order_line:
                template_ids = self.env['pos.term.template'].sudo().search([('product_ids','in', each.product_id.id)])
                for tmpl in template_ids:
                    if tmpl not in template_lst:
                        template_lst.append(tmpl)
        if template_lst:
            return template_lst
        else:
            return False



    def get_term_template(self):
        template_lst = []
        if self.order_line:
            for each in self.order_line:
                template_ids = self.env['pos.term.template'].sudo().search([('product_ids','in', each.product_id.id)])
                for tmpl in template_ids:
                    if tmpl not in template_lst:
                        template_lst.append(tmpl)
        if template_lst:
            return template_lst
        else:
            return False

