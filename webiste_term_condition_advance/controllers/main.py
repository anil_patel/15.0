# -*- coding: utf-8 -*-

from odoo import http
import logging
import base64
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from werkzeug.exceptions import Forbidden, NotFound
_logger = logging.getLogger(__name__)
from odoo import fields, http, SUPERUSER_ID, tools, _
from odoo.exceptions import ValidationError
from odoo.http import request, Response, JsonRequest
import json
from datetime import date,datetime


class WebsiteSale(WebsiteSale):

    @http.route(['/check/term/'], type='json', auth="public", methods=['POST'] , website=True)
    def check_pos_term_condition(self, order):
        order_id = request.env['sale.order'].sudo().browse(int(order))
        res = False
        if order_id:
            res = order_id.check_term_condition_required()
        return res

    @http.route(['/accept/pos/term/'], type='json', auth="public", methods=['POST'] , website=True)
    def accept_pos_term_conditions(self, sale_id, accept_date, template_id):
        sale_id = request.env['sale.order'].sudo().search([('id','=',int(sale_id))])
        if sale_id:
            date  = datetime.strptime(accept_date, '%m-%d-%Y')
            sale_id.write({'pos_term_accept': True, 'pos_term_accept_date': date})
            if sale_id.term_templates:
                sale_id.term_templates += ','+template_id
            else:
                sale_id.term_templates = template_id
        return True

    @http.route(['/get/term/template/'], type='json', auth="public", methods=['POST'] , website=True)
    def send_term_condition_data(self, template_id):
        template_id = request.env['pos.term.template'].browse(int(template_id))
        return {'name': template_id.name, 'term': template_id.term}

 