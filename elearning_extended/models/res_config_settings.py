# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api


class ResConfigSettingsInherit(models.TransientModel):
    _inherit = "res.config.settings"

    module_is_student = fields.Boolean(string="Is Student")

    @api.model
    def get_values(self):
        res = super(ResConfigSettingsInherit, self).get_values()
        module_is_student = self.env['ir.config_parameter'].sudo().get_param('elearning_extended.module_is_student')
        if module_is_student == 'False':
            res['module_is_student'] = False
        else:
            res['module_is_student'] = True
        return res

    @api.model
    def set_values(self):
        if self.module_is_student == False:
            self.env['ir.config_parameter'].sudo().set_param('elearning_extended.module_is_student', 'False')
        else:
            self.env['ir.config_parameter'].sudo().set_param('elearning_extended.module_is_student','True')
            irModuleObj = self.env['ir.module.module']
            irModuleObj.update_list()
            moduleIds = irModuleObj.search([('state', '!=', 'installed'),
                                            ('name', '=', 'elearning_student_signup')], limit=1)
            if moduleIds:
                moduleIds.button_immediate_install()
                # moduleIds.button_immediate_upgrade()
        super(ResConfigSettingsInherit, self).set_values()
