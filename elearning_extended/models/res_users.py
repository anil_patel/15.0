from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import AccessDenied, AccessError, UserError, ValidationError


class Users(models.Model):
    _inherit = "res.users"
    
    res_users_categ_id = fields.Many2one('res.users.categ','Users Category')


class UsersCateg(models.Model):
    _name = 'res.users.categ'
    _rec_name = 'complete_name'
    _order = 'complete_name'
    
    name = fields.Char('Name')
    parent_id = fields.Many2one('res.users.categ','Parent Category')
    complete_name = fields.Char(
        'Complete Name', compute='_compute_complete_name',
        store=True)

    @api.depends('name', 'parent_id.complete_name')
    def _compute_complete_name(self):
        for category in self:
            if category.parent_id:
                category.complete_name = '%s / %s' % (category.parent_id.complete_name, category.name)
            else:
                category.complete_name = category.name
