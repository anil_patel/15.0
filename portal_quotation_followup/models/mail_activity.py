# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from datetime import datetime, date, timedelta

class MailMessage(models.Model):
    _inherit = 'mail.message'

    @api.model
    def create(self, vals):
        res = super(MailMessage, self).create(vals)
        if res.model and res.model == 'sale.order':
            if "Quotation viewed by customer" in res.with_context(lang='en_US').body:
                sale_id = self.env['sale.order'].sudo().browse(res.res_id).sudo()
                if sale_id.user_id and sale_id.user_id:
                    activity_type_id = self.env.ref('mail.mail_activity_data_todo')
                    date_deadline = date.today() + timedelta(days=1)
                    model_id = self.env['ir.model']._get('sale.order').id
                    activity_id = self.env['mail.activity'].sudo().create({
                        'activity_type_id': activity_type_id.id,
                        'date_deadline': date_deadline,
                        'summary': 'Followup Quotation',
                        'note': 'Quotation view by customer from portal.',
                        'user_id': sale_id.user_id.id,
                        'res_model_id': model_id,
                        'res_id': sale_id.id,
                        'res_model': 'sale.order'
                    })
        return res

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.model
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        if self.user_id and self.user_id and self.signature:
            activity_type_id = self.env.ref('mail.mail_activity_data_todo')
            date_deadline = date.today() + timedelta(days=1)
            model_id = self.env['ir.model']._get('sale.order').id
            activity_id = self.env['mail.activity'].sudo().create({
                'activity_type_id': activity_type_id.id,
                'date_deadline': date_deadline,
                'summary': 'Quotation Signed',
                'note': self.name +' '+'signed by' +' ' +self.signed_by ,
                'user_id': self.user_id.id,
                'res_model_id': model_id,
                'res_id': self.id,
                'res_model': 'sale.order'
            })
        return res