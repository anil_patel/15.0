# -*- coding: utf-8 -*-

from odoo import SUPERUSER_ID, _, api, fields, models

PROCUREMENT_PRIORITIES = [('0', 'Normal'), ('1', 'Urgent')]


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def action_confirm(self):
        super(SaleOrder, self).action_confirm()
        if self.picking_ids :
            for picking in self.picking_ids:
                is_lot_serial_product = False
                if picking.move_ids_without_package:
                    for line in picking.move_ids_without_package:
                        if line.product_id.tracking in ['lot','serial']:
                            is_lot_serial_product = True
                if is_lot_serial_product:
                    picking.do_unreserve()


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def write(self, vals):  
        if self.order_id.state == 'sale' and vals.get('product_uom_qty'):
            self.env.context = dict(self.env.context)
            self.env.context.update({'from_sol_qty_change': True})
        return super(SaleOrderLine, self).write(vals)


class Picking(models.Model):
    _inherit = "stock.picking"

    def _create_backorder(self):
        """ This method is called when the user chose to create a backorder. It will create a new
        picking, the backorder, and move the stock.moves that are not `done` or `cancel` into it.
        """
        backorders = self.env['stock.picking']
        for picking in self:
            moves_to_backorder = picking.move_lines.filtered(lambda x: x.state not in ('done', 'cancel'))
            if moves_to_backorder:
                backorder_picking = picking.copy({
                    'name': '/',
                    'move_lines': [],
                    'move_line_ids': [],
                    'backorder_id': picking.id
                })
                picking.message_post(
                    body=_('The backorder <a href=# data-oe-model=stock.picking data-oe-id=%d>%s</a> has been created.') % (
                        backorder_picking.id, backorder_picking.name))
                moves_to_backorder.write({'picking_id': backorder_picking.id})
                moves_to_backorder.move_line_ids.package_level_id.write({'picking_id': backorder_picking.id})
                moves_to_backorder.mapped('move_line_ids').write({'picking_id': backorder_picking.id})
                backorders |= backorder_picking
        # if backorders:
        #     backorders.action_assign()
        return backorders


    def action_assign(self):
        if self._context.get('from_sol_qty_change'):
            return True
        res = super(Picking, self).action_assign()
        if self.picking_type_code == 'internal' or self.picking_type_code == 'outgoing':
            lst = []
            reserved_move_lines = self.move_line_ids.filtered(lambda l:l.product_id.tracking in ['serial','lot'] and l.product_uom_qty)
            unreserve_move_lines = self.move_line_ids.filtered(lambda l:l.product_id.tracking in ['serial','lot'] and not l.product_uom_qty)
            for each in self.move_line_ids:
                if each.lot_id:
                    each.delivery_lot_name = each.lot_id.name
            if reserved_move_lines:
                for each in reserved_move_lines:
                    if each.lot_id and unreserve_move_lines:
                        unreserve_line = unreserve_move_lines.filtered(lambda l:l.lot_id == each.lot_id)
                        if unreserve_line:
                            lst.append(unreserve_line)
            if lst:
                for each in lst:
                    each.sudo().unlink()
        return res