# -*- coding: utf-8 -*-
{
    "name": "Scan Multiple Serial Number & Stop Auto Lot/Serial Selection",
    "author": "Valueble IT Solution",
    "support": "anil.patel13688@gmail.com",
    'website': 'valuebleitsolution.odoo.com',
    "category": "Warehouse",
    "summary": """This module allow to scan multiple serial number in odoo delivery order and internal transfer and also stop the "
               "auto selection of lot and serial number. you can scan multiple serial number at a time and it will auto set all the move lines autometically this "
               "help to perform quick operation and save the user time.""",
    "description": """
    multi scan serial number,
    Multi scan lot serial,
    stop auto lot selection,
    stop auto lot/serial,
    scan serial number on delivery order,
    """,
    "version": "14.0.1",
    'license': 'AGPL-3',
    "depends": [
        'sale_management',
        'stock',
    ],
    "data": [
        'views/stock_move_line.xml'
    ],
    'images': ['static/description/img4.png'],
    "auto_install": False,
    "application": True,
    "installable": True,
    "price": "50",
    "currency": "EUR"
}
