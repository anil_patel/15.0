# -*- coding: utf-8 -*-

from odoo import api, models, fields,_
from odoo.exceptions import UserError, ValidationError
import logging
import re
import traceback
import time # Import whole time module
from TextMagic.rest import ApiException

from odoo import api, fields, models, _
import datetime


class TextmagicLogs(models.Model):
    _name = 'textmagic.logs'
    _description = "Textmagic Logs"
    _rec_name = 'date'

    date = fields.Date(string="Date")
    sms = fields.Text(string="SMS")
    line_ids = fields.One2many('textmagic.logs.line', 'log_id')


class TextmagicLogsLine(models.Model):
    _name = 'textmagic.logs.line'
    _description = "Textmagic Logs Lines"

    partner_id = fields.Many2one('res.partner', string="Contact")
    result = fields.Text(string="Status")
    log_id = fields.Many2one('textmagic.logs', string="log id")


class TextmagicGateway(models.Model):
    _name = 'textmagic.gateway'
    _description = "Textmagic Configuration"

    username = fields.Char(string="Username")
    password = fields.Char(string="Password")